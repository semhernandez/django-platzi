from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^logout$', auth_views.logout, {'next_page':'/'}, name='logout'),
    url(r'^login/$', views.auth_login, name='autentication'),
    url(r'^$', views.ProductListView.as_view(), name='hola'),
    #url(r'^product/(?P<pk>[0-9]+)/$', views.product_detail, name='product_detail'),
    url(r'^product/(?P<pk>[0-9]+)/$', views.ProductDetail.as_view(), name='product_detail'),
    url(r'^product/new', views.new_product, name='new_product')
]
